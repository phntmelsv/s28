db.hotel.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description:"A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});


// To check if the document was created
// db.getCollection('hotel').find({})

// ask if the details should be changed for some documents / entries?
db.hotel.insertMany([
	{
		name: "double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on vacation",
		rooms_available: 5,
		isAvailable: false
	}
]);

db.hotel.insertMany([
	{
		name: "queen",
		accommodates: 4,
		price: 4000,
		description: "A room fit with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
]);


// find
db.hotel.find({name: "double"});

// updateOne
db.hotel.updateOne(
	{name: "queen"},
	{
		$set:{
			rooms_available: 0
		}
	}
);

// deleteMany
db.hotel.deleteMany({rooms_available:0});

